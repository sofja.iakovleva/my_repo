package my_repo.java;

public class AverageMassive{
  public static void main(String[] args) {

    int[] massive = new int[24];
    int summ = 0;

    for (int i = 0; i < massive.length; i++) {
      massive[i]= ((int)(Math.random()* 200)-100);
      summ += massive[i];
      System.out.println(massive[i]);
    }

    double average = (summ / massive.length);
    System.out.println("average: " + average);

    int element = massive[0];
    double minim = Math.abs(average - massive[0]);
    for (int j = 0; j < massive.length; j++) {
      if (Math.abs(average - massive[j]) < minim) {
        minim = (Math.abs(average - massive[j]));
        element = massive[j];
      }
    }
    System.out.println("element: "  + element);
  }
}

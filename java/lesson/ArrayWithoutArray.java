package my_repo.lesson;

class ArrayWithoutArray implements java.until.Iterator<Integer> {
  private int current = 0;
  private int limit;

  ArrayWithoutArray(int limit){
    this.limit = limit;
  }
  public boolean hasNext() {
    return this.current <= this.limit;
  }
  public Integer next() {
    int value = this.current;
    this.current++;
    return value
  }
}

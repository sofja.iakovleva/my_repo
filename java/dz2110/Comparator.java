package my_repo.java.dz2110;

import java.util.Comparator;

public class Comp implements Comparator<Nomer_3> {
    public int compare(Nomer_3 st1, Nomer_3 st2) {
        if (st1.getName().length() - st2.getName().length() > 0)
            return 1;
        else if (st1.getName().length() - st2.getName().length() == 0)
            return 0;
        else
            return -1;
    }
}

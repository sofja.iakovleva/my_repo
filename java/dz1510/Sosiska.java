package my_repo.dz1510;

public class Sosiska {
    public int whiteZarp;
    private int blackZarp;

    public void setBlackZarp(int blackZarp) {
        this.blackZarp = blackZarp;
    }

    public int getWhiteZarp() {
        return whiteZarp;
    }

    public void setWhiteZarp(int whiteZarp) {
        this.whiteZarp = whiteZarp;
    }

    public String toString() {
        return "Зарплата: " + (whiteZarp + blackZarp);
    }
}

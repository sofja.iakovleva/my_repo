package my_repo.java;

import java.util.Scanner;

public class FactorialRekursia{
	static int factorialrekursia(int a){
		if (a==1){
			return 1;
		}
		return a*factorialrekursia(a-1);
	}
	public static void main(String[] args){
		Scanner scanner =  new Scanner(System.in);
		int a;
		System.out.println("Введите число для вычисления факториала: ");
		a=scanner.nextInt();
		System.out.println(factorialrekursia(a));
	}
}

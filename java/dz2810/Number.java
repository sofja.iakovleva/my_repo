public abstract class Number{
  byte byteValue();
  abstract double doubleValue();
  abstract float floatValue();
  abstract int intValue();
  abstract long longValue();
  short shortValue();
}

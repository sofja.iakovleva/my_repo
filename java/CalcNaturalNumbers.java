package my_repo.java;

import java.util.Scanner;

public class CalcNaturalNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите число n: ");
        int n = scanner.nextInt();
        long sumN = 0;
        for (int i = 1; i <= n; i++) {
          sumN = sumN + i;
          }
System.out.println("Сумма первых n натуральных чисел: " + sumN);
}
}

package my_repo.java.dz2610;

public class Osnova {
  public static void main(String[] args) {

    System.out.println("1:");
    Kubshka[] a = new Kubshka[3];
    a[0] = new Kubshka("Кубышка_1");
    a[1] = new Kubshka("Кубышка_2");
    a[2] = new Kubshka("Кубышка_3");
    for (int i = 0; i < a.length; i++) {
        System.out.println(a[i].name + ". Денег: " + a[i].num() + "k рублей");
    }
    System.out.println();
    System.out.println("2:");

    Student[] student = new Student[3];
    student[0] = new Student("Яковлева", "Софья", "Вячеславовна", 10);
    student[1] = new Student("Благославов", "Михаил", "Сергеевич", 19);
    student[2] = new Student("Чечеткина", "Елена", "Николаевна", 35);
    for (int i = 0; i < student.length; i++) {
        System.out.println("Фамилия: " + student[i].famil + ". Имя: " + student[i].name + ". Отчество: " + student[i].otches + ". Результат: " + student[i].result + "сек.");
    }
  }
}

package my_repo.java.dz2610;

public class Student {

  public String familia;
  public String name;
  public String otchestvo;
  public int resultat;

  public Student(String familia, String name, String otchestvo, int resultat) {
    this.familia = familia;
    this.name = name;
    this.otchestvo = otchestvo;
    this.resultat = resultat;
  }
}

package my_repo.practic;

class Solution3 {
  public static void main(String[] args) {
    int[] a = {1,2,3,1,5,6,7,8,9,10};
    int robot  = 1;
    int max1 = 0;
    for (int i = 0; i < a.length - 1; i++) {
      if (a[i] + 1 == a[i+1]) {
	robot++;
        if (robot > max1){
          max1 = robot;
        }
      }
      if (a[i] + 1 != a[i+1]) {
        robot = 1;
      }
    }
    System.out.println(max1);
  }
}

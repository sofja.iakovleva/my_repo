package ru.sirius.main.exceptions;
import java.util.*;

class Program {
  static int div(int a, int b) {
    return a / b;
  }

  static void doSmth() {
    System.out.println("doing something");
    int[] a = {1};
    System.out.println(a[2]);
  }

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    int a = sc.nextInt();
    int b = sc.nextInt();

    try {
      System.out.println(div(a, b));
      doSmth();
    } catch (ArithmeticException e) {
      System.out.println("ERROR 1");
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("ERROR 2");
    }
  }
}

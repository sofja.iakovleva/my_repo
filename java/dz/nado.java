package my_repo.tasks;

class Tasks_4_program {
  public static void main(String[] args) {
  Tasks_4oop o1 = new Tasks_4oop();
  Tasks_4oop o2 = new Tasks_4oop();

  System.out.println(o1);
  System.out.println(o1.myField1);
  System.out.println(o1.myField2);
  
  o1.myField1 = 123;
  o1.myField2 = 345;
  o1.setMyField3(90);

  System.out.println(o1.myField1);
  System.out.println(o1.myField2);
  System.out.println(o1.getMyField3());


  System.out.println(o2);
  System.out.println(o2.myField1);
  System.out.println(o2.myField2);
  }
}

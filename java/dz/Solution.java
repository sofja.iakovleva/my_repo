
package my_repo.pracric;

class Solution {
  
  public int myField1; // переменные экземпляра
  public int myField2;
  private int myField3;

  Solution() {}
  Solution(int myField1, String myField2, int myField3) {
  this.myField1 = myField1;
  this.myField2 = myField2;
  // this.myField3 = myField3
  this.setMyField(myField3);
  }

  public int getMyField3() { //получает
    return myField3;
  }

  public void setMyField3(int myField3) { //устанавливает
    this.myField3 = myField3;
  
  }
}

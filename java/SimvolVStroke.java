package my_repo.java;

import java.util.Scanner;

public class SimvolVStroke{
  public static void main(String[] args){
    System.out.println("Введите строку");
    Scanner scanner = new Scanner(System.in);
    String stroka = scanner.nextLine();
    char simvol = 't';
    int kolichestvo = 0;
    char[] v_stroka = stroka.toCharArray();
    for (int i=0; i<v_stroka.length; i++){
        if (v_stroka[i] == simvol){
          kolichestvo+=1;
        }
    }
    System.out.format("Символ t встречается в строке %d раз", kolichestvo);
  }
}

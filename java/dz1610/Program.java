package my_repo.dz1610;

class Program {
  public void run() throws InterruptedException {
    while(true) {
      double value = Math.random();
      System.out.println(value);
      this.wait(1);
    }
  }
  public static void main(String[] args) throws InterruptedException {
    Program p = new Program();
    p.run();
  }
}

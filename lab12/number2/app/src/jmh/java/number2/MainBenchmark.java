
/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package number2;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.annotations.Level;

import java.util.Map;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.TreeMap;
import java.util.ArrayList;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
public class MainBenchmark{
  Maps map1 = new Maps(new HashMap<String, Integer>());
  Maps map2 = new Maps(new TreeMap<String, Integer>());
  Maps map3 = new Maps(new LinkedHashMap<String, Integer>());
  Maps map4 = new Maps(new Hashtable<String, Integer>());

  @Setup(Level.Trial) @Benchmark
  public void testCreateHashMap(){
    Maps hashMap = new Maps(new HashMap<String, Integer>());
  }

  @Setup(Level.Trial) @Benchmark
  public void testCreateTreeMap(){
    Maps treeMap = new Maps(new TreeMap<String, Integer>());
  }

  @Setup(Level.Trial) @Benchmark
  public void testCreateLinkedHashMap(){
    Maps linkedHashMap = new Maps(new LinkedHashMap<String, Integer>());
  }

  @Setup(Level.Trial) @Benchmark
  public void testCreateHashtable(){
    Maps hashtable = new Maps(new Hashtable<String, Integer>());
  }

  @Setup(Level.Trial) @Benchmark
  public void testSumHashMap(){
    map1.sum(map1.mass());
  }

  @Setup(Level.Trial) @Benchmark
  public void testSumTreeMap(){
    map2.sum(map2.mass());
  }

  @Setup(Level.Trial) @Benchmark
  public void testSumLinkedHashMap(){
    map3.sum(map3.mass());
  }

  @Setup(Level.Trial) @Benchmark
  public void testSumHashtable(){
    map4.sum(map4.mass());
  }
}

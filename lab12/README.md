*Задание*:

Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Вывести значения отсортированные по возрастанию.
Сравнить скорость работы для разных базовых классов.

[Эксель таблица](https://docs.google.com/spreadsheets/d/11Fq6-nMzMCmeW916I41Eb2ewt5GWXkhASmuOBm4lUEQ/edit?usp=sharing)

[Диаграмма классов](https://drive.google.com/file/d/1U4Qd6NngpJpm4utB1WWXwEg40n7SmOLy/view?usp=sharing)

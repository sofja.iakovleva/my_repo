/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package lab15;


import java.util.Random;
import java.io.Serializable;

/**
 * Class Student for creating a student with
 * random name and course
 * @author Sofia Iakovleva
 * @version 1.0
 * @see Main
 */
public class Folder implements Serializable{

  /**
  @value unique identifier class for version control
  */
   private static final long serialVersionUID = -5677196060428920309L;

  /**
  @value the field that stores the folder's name
  */
  private String name;

  /**
  @value the field that stores the folder's size
  */
  private int size;

  /**
   * constructor Folder
   */
  Folder(){
    this.name = nameGeneration();
    this.size = sizeGeneration();
  }

  /**
   * method getName
   * @return folder's size
   */
  public String getName(){
    return this.name;
  }

  /**
   * method getSize
   * @return folder's size
   */
  public int getSize(){
    return this.size;
  }

  /**
  * method to generating a string with random characters
  * @return generated string uppercase
  */
  public String nameGeneration(){
    Random a = new Random();
    String str = "";
    for (int i = 0; i < 10; i++) {
        str += (char) (a.nextInt(26) + 'a');
    }
    return str.substring(0,1).toUpperCase() + str.substring(1);
  }

  /**
  * method to generating random number
  * @return generated number
  */
  public int sizeGeneration(){
    int value = 0;
    for (int i = 0; i < 100; i++){
      value = (int)(Math.random() * (6) + 1);
    }
    return value;
  }

  /**
  * override toString method
  * @return string with student data
  */
  public String toString(){
    return getName() + " " + getSize();
  }
}

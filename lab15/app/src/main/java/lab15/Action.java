/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package lab15;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.Random;
import java.util.Scanner;

/**
 * Class Action for creating, serializing and deserializing
 * map with generating keys and values
 * @author Sofia Iakovleva
 * @version 1.0
 * @see Main
 */
public class Action{
  /**
   * method for map creation and filling
   * @return map class instance
   */
  public static HashMap<Integer, Folder> mapGeneration(){

    HashMap<Integer, Folder> map = new HashMap<>();

    for (int i = 0; i < 100; i++){
      int keys = (int)(Math.random() * 1000);
      map.put(keys, new Folder());
    }
    return map;
  }

  /**
   * method map serialization
   * @param filename - path of file
   */
  public static void serialize(String filename) {

    FileOutputStream fos = null;
    ObjectOutputStream out = null;

    try{
      fos = new FileOutputStream(filename);
      out = new ObjectOutputStream(fos);
      out.writeObject(mapGeneration());
      out.close();
      System.out.println("HashMap");
    }
    catch(IOException ex){
      ex.printStackTrace();
    }
  }

  /**
   * method map deserialization
   * @param filename - path of file
   */
  public static void deserialize(String filename){

    HashMap<Integer, Folder> map = null;
    FileInputStream fis = null;
    ObjectInputStream in = null;

    try {
      fis = new FileInputStream(filename);
      in = new ObjectInputStream(fis);
      map = (HashMap<Integer, Folder>)in.readObject();
      in.close();
      System.out.println("HashMap");
    }
    catch(IOException ex){
      ex.printStackTrace();
    }
    catch(ClassNotFoundException ex){
      ex.printStackTrace();
    }

    Set set = map.entrySet();
    Iterator iterator = set.iterator();

    while (iterator.hasNext()) {
        Map.Entry entry = (Map.Entry)iterator.next();
        System.out.println("Key : " + entry.getKey() + " and value : " + entry.getValue());
    }
  }
}

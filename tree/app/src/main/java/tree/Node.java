/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package tree;

public class Node<E>{
   public E data;
   public Node<E> parent;
   public Node<E>[] children;

   public Node(){

   }

   public Node(E data, Node<E> parent, Node<E>[] children){
      this.data = data;
      this.parent = parent;
      this.children = children;
   }

   public E getData(){
      return this.data;
   }

   public void setData(E data){
      this.data = data;
   }

   public Node<E> getParent(){
      return this.parent;
   }

   public void setParent(Node<E> parent){
      this.parent = parent;
   }

   public Node<E>[] getChildren(){
      return this.children;
   }

   public int getChildrenLength(){
     return this.children.length;
   }

   public boolean isLeaf(){
     return getChildrenLength() == 0;
   }

   public boolean isRoot(){
     return this.parent == null;
   }

   public int getLevel(){
     if (isRoot()){
       return 0;
     }
     else{
       return this.parent.getLevel() + 1;
     }
   }
}

/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package number3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void string1() {
      String text = "I loved to eat spagetti";
      String[] replaces = new String[] {"spagetti", "cookies"};
      String loveWord = "cakes";
      assertEquals(DontLove.check(text, replaces, loveWord), "I loved to eat cakes");
    }

    @Test void string2() {
      String text = "I loved to eat cookies";
      String[] replaces = new String[] {"spagetti", "cookies"};
      String loveWord = "cakes";
      assertEquals(DontLove.check(text, replaces, loveWord), "I loved to eat cakes");
    }
}

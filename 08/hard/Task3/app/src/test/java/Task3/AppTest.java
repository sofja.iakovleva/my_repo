/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Task3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test void testSuccess() {
      App generator = new App(100, 5);
      assertTrue(generator.getSuccess());
      assertEquals(100, generator.getNumbers().length);
    }


    @Test void testDefeat(){
      App generator = new App(1000000, 5);
      assertNull(generator.getNumbers());
    }
}

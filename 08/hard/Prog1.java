
/*Создать программу, позволяющую вводить с клавиатуры число,
сохранять его в целочисленной переменной и затем выводить переменную на консоль.
Надо учесть, что пользователь может ввести любую строку.
Если среди значащих символов есть только цифры, + или -, то строка должна быть преобразована в число.*/

package my_repo.hard;
import java.util.Scanner;


class Prog1{
  public static void main(String[] args){
    Scanner scanner = new Scanner(System.in);
    int foy = 0;
    System.out.println("Введите число: ");
    if (scanner.hasNextInt()) {
      foy = scanner.nextInt();
    } else {
      System.out.println("Это не число! Ты что-то путаешь! Перезапусти программу!");
    }
    scanner.close();
    if (foy != 0){
      System.out.println(foy);
    }
  }
}

/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Prog6;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void multiplication() {
    int[][] matrix1 = {{1, 3, 9}, {8, 2, 6}};
    int[][] matrix2 = {{4, 3}, {1, 3}, {2, 5}};
    int[][] matrix3 = operation.multiplication(matrix1, matrix2);
    int[][] matrix4 = {{25, 57}, {46, 60};
    assertArrayEquals(matrix4, matrix3);
  }
}
